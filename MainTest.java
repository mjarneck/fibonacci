import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void silnia() {
        Main main = new Main();
        int result = main.silnia(5);
        Assert.assertEquals(120, result);
        Assert.assertEquals(24, main.silnia(4));
    }

    @Test
    public void fibonacci() {
        Main main1 = new Main();
        int result = main1.fibonacci(5);
        Assert.assertEquals(5, result);
        Assert.assertEquals(34, main1.fibonacci(9));
        Assert.assertEquals(4181, main1.fibonacci(19));
    }

    @Test
    public void tribonacci() {
        Main main2 = new Main();
        int result = main2.tribonacci(7);
        Assert.assertEquals(13, result);
        Assert.assertEquals(81, main2.tribonacci(10));
        Assert.assertEquals(19513, main2.tribonacci(19));
    }

}