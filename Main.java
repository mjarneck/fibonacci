public class Main {


    public int silnia(int number){
        if(number == 1){
            return 1;
        }else{
            return silnia(number-1)*number;
        }
    }

    public int fibonacci(int fib_num){
        if((fib_num == 1) || (fib_num == 2)) {
            return 1;
        }else{
            return fibonacci(fib_num-1)+fibonacci(fib_num-2);
        }
    }

    public int tribonacci(int trib_num){
        if(trib_num == 1){
            return 0;
        }else if ((trib_num == 2) || (trib_num == 3)) {
            return 1;
        }else{
            return tribonacci(trib_num-1)+tribonacci(trib_num-2)+tribonacci(trib_num-3);
        }
    }
}
